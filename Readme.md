# Prerequis

1. [Installer Docker](https://docs.docker.com/install/) sur votre machine
2. [Installer NVIDIA Support docker](https://github.com/NVIDIA/nvidia-docker) (optionnel, uniquement pour Linux)

# Contruire l'image docker 

Si vous souhaitez partir d'une image tensorflow en particulier, éditez le fichier build.sh du repertoire script et modifier la valeur de la variable $tensorflow_version$. Par exemple :
> tensorflow_version='latest-gpu'

Depuis le répertoire racine (le repertoire dans lequel se trouve le fichier Dockerfile), executez le script build :
> './scripts/build.sh'

# Lancer le container

Avant de lancer le container, il faut vérifier qu'il y a bien un répertoire to_include/workspace (pour y mettre les scripts python) et un autre repertoire to_include (dans lequel vous y mettrez les notebooks dont vous avez besoin). 
Tout ce qui est sauvegardé dans ces repertoires est conservé sur la machine hôte lorsque vous quittez le conintainer.

Executez le scrpt run :
> './scripts/run.sh'

Pour utiliser jupyter notebook, lancez la commande : 
> jupyter notebook --notebook-dir=../notebooks/ --no-browser --ip 0.0.0.0 --allow-root

Puis copier/coller le lien qui s'affiche dans un navigateur.

Vous pouvez également arréter le serveur jupyter (Ctrl+c) et récupérer le controle du terminal.

