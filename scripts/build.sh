# Change the tensorflow_version according to your needs (e.g. to use gpu)
# (cf tags on https://hub.docker.com/r/tensorflow/tensorflow/) 

tensorflow_version='latest'

echo "🛠️  Build dockerfile" >&2
docker build --build-arg ENV=$tensorflow_version --tag dlp:latest .


