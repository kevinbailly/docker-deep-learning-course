ARG ENV
FROM tensorflow/tensorflow:$ENV

COPY requirements/run.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR ./workspace/
